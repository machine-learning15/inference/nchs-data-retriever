package works.mars.ml.etl.nchs.app;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.*;
import java.util.zip.ZipEntry;

/**
 * Downloads files from CDC given by ftp urls, unzips them, then writes to a folder.
 */
public class NCHSDataDownloader {

    private final Path compressedDirPath;
    private final Path uncompressedDirPath;

    public NCHSDataDownloader(final Path uncompressedDirPath, final Path compressedDirPath) {

        this.uncompressedDirPath = uncompressedDirPath;
        this.compressedDirPath = compressedDirPath;

    }

    /**
     * Validate and return URL
     *
     * @param ftpUrl
     * @return validated URL
     * @throws MalformedURLException
     */
    URL getURL(String ftpUrl) throws MalformedURLException {

        URL url = new URL(ftpUrl);
        if (!url.getProtocol().equals("ftp")) {
            throw new IllegalArgumentException(String.format("Provided protocol: %s, expected: ftp", url.getProtocol()));
        }
        if (!url.getHost().equals("ftp.cdc.gov")) {
            throw new IllegalArgumentException(String.format("Provided host: %s, expected: ftp.cdc.gov", url.getHost()));
        }

        Path fileName = Path.of(url.getPath()).getFileName();
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*.zip");
        if (!matcher.matches(fileName)) {
            throw new IllegalArgumentException(String.format("Provided file extension: %s, expected: .zip", fileName));
        }

        return url;
    }

    /**
     * Downloads and un-compresses file.
     *
     * @param ftpUrl
     * @return The path of the uncompressed file
     */
    public Path downloadAndUncompress(String ftpUrl) throws IOException {

        URL url = getURL(ftpUrl);
        Path compressedFilePath = Path.of(compressedDirPath.toString(), Path.of(url.getPath()).getFileName().toString());
        Path outputFilePath = null;

        try (InputStream inputStream = url.openStream()) {
            Files.copy(inputStream, compressedFilePath);
        }

        // uncompress and write file
        try (java.util.zip.ZipFile zf = new java.util.zip.ZipFile(compressedFilePath.toString())) {
            for (java.util.Enumeration entries = zf.entries(); entries.hasMoreElements(); ) {

                ZipEntry zipEntry = (ZipEntry) entries.nextElement();
                outputFilePath = Path.of(uncompressedDirPath.toString(), zipEntry.getName());

                try (InputStream inputStream = zf.getInputStream(zipEntry)) {
                    Files.copy(inputStream, outputFilePath, StandardCopyOption.REPLACE_EXISTING);
                }
            }
        }

        return outputFilePath;
    }

}
