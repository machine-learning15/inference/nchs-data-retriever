package works.mars.ml.etl.nchs.app;

import java.util.Collection;
import java.util.Map;

public interface RecordWriter {

    /**
     * Insert Records to a datastore
     *
     * @param records
     * @return
     */
    boolean insertRecords(Collection<Map<String, String>> records);
}
