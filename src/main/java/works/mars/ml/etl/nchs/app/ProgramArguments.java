package works.mars.ml.etl.nchs.app;

import picocli.CommandLine;

import java.util.List;

public class ProgramArguments {

    @CommandLine.Parameters(paramLabel = "--ftp-file-urls", description = "Space delimited ftp urls")
    List<String> ftpFileUrls;

    @CommandLine.Option(names = "--path-to-metadata-file", required = true, description = "File containing field names and their start and end indices.  If the indices start do not start counting from 0, set --startCountingFromOne")
    String pathToMetadataFile;

    @CommandLine.Option(names = "--batch-size", required = true, description = "Number of records that will be processed in batch")
    Integer batchSize;

    @CommandLine.Option(names = "--dataset-name", required = true, description = "BigQuery dataset name, if dataset does not exist, it will be created")
    String datasetName;

    @CommandLine.Option(names = "--table-name", required = true, description = "BigQuery table name where records are written in batch, if table does not exists, it will be created")
    String tableName;

    @CommandLine.Option(names = "--expectedFieldCount", defaultValue = "223", description = "Number of fields in metadata file")
    Integer expectedFieldCount;

    @CommandLine.Option(names = "--startCountingFromOne", description = "Whether the indices given in the metadata file start counting from 0")
    Boolean startCountingFromOne;

}

