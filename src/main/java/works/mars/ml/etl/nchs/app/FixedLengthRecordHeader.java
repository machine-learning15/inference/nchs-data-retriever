package works.mars.ml.etl.nchs.app;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.*;


public class FixedLengthRecordHeader {

    private ArrayList<FixedLengthField> myFields;
    private Boolean useZeroBasedCounting;


    public FixedLengthRecordHeader(String fileStructureFile, Boolean startCountingFromZero) throws IOException {

        this.useZeroBasedCounting = startCountingFromZero;
        File file = new File(fileStructureFile);
        TypeReference<HashMap<String, Integer[]>> typeRef = new TypeReference<>() {};
        Map<String, Integer[]> fieldStructure = new ObjectMapper().readValue(file, typeRef);

        this.myFields = new ArrayList<>();
        constructFields(fieldStructure);
    }

    // created for test
    public FixedLengthRecordHeader(Map<String, Integer[]> fieldStructure) {

        this.myFields = new ArrayList<>();
        constructFields(fieldStructure);
    }

    public Integer getCount() {
        return myFields.size();
    }

    private void constructFields(Map<String, Integer[]> fieldStructure) {
        for (String fieldName : fieldStructure.keySet()) {
            FixedLengthField field = new FixedLengthField(fieldName, fieldStructure.get(fieldName)[0], fieldStructure.get(fieldName).length == 1 ? fieldStructure.get(fieldName)[0] : fieldStructure.get(fieldName)[1]);
            myFields.add(field);
        }

        Collections.sort(myFields);

        for (int i = 0; i < myFields.size() - 1; i++) {
            if (myFields.get(i + 1).getStart() <= myFields.get(i).getEnd()) {
                throw new IllegalArgumentException(String.format("Overlapping fields are detected.  Field = %s , Field = %s", myFields.get(i), myFields.get(i + 1)));
            }
        }

        System.out.printf("Constructed fields = %s%s", myFields, System.lineSeparator());
    }

    /**
     * Parses a String line to fields and returns a FieldName->FieldValue map
     *
     * @param line
     * @return
     */
    public Map<String, String> parseToFields(String line) {
        Map<String, String> result = new HashMap<>();
        for (FixedLengthField field : myFields) {
            String name = field.getName();
            String value = line.substring(useZeroBasedCounting ? field.getStart() : field.getStart() - 1, useZeroBasedCounting ? field.getEnd() + 1 : field.getEnd());
            result.put(name, value.trim().length() == 0 ? null : value.trim());
        }

        return result;
    }

    public List<FixedLengthField> getFields() {
        return this.myFields;
    }
}
