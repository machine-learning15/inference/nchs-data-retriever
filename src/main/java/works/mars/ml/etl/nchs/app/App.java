package works.mars.ml.etl.nchs.app;

import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

/**
 * 1 - Downloads files from ftp servers; it is assumed that files are compressed in .zip format
 * 2 - Un-compresses files and writes them in a temp directory
 * 3 - Creates schema from metadata file, validates that there is no overlap between fields, e.g same block of the line is occupied by more than one field, then uses this schema to parse each line given a start and an end index
 * 4 - Writes lines in batch to BigQuery. If given dataset and table does not exist, it creates them with a schema.  All fields are of type STRING and NULLABLE.
 * 5 - Cleans up temp directories
 */
public class App {

    public static void main(String[] args) throws IOException {

        ProgramArguments programArguments = new ProgramArguments();
        new CommandLine(programArguments).parseArgs(args);

        // create temp directories to store compressed and uncompressed files
        Path compressedDirPath = Path.of(Files.createTempDirectory("compressed").toFile().getAbsolutePath());
        Path uncompressedDirPath = Path.of(Files.createTempDirectory("uncompressed").toFile().getAbsolutePath());

        // Construct field structure
        FixedLengthRecordHeader fixedLengthRecordHeader = new FixedLengthRecordHeader(programArguments.pathToMetadataFile, !programArguments.startCountingFromOne);
        assert fixedLengthRecordHeader.getCount().equals(programArguments.expectedFieldCount) : "Expected field count does not match processed field count = " + fixedLengthRecordHeader.getCount();

        // Construct BigQuery Client
        final RecordWriter bigQueryWriter = new BigQueryWriter(
                programArguments.datasetName,
                programArguments.tableName,
                fixedLengthRecordHeader.getFields());

        // Download, uncompress, write to BigQuery
        NCHSDataDownloader dataDownloader = new NCHSDataDownloader(uncompressedDirPath, compressedDirPath);

        for (String ftpUrl : programArguments.ftpFileUrls) {
            System.out.printf("Processing file = %s%s", ftpUrl, System.lineSeparator());
            Path uncompressedFile = dataDownloader.downloadAndUncompress(ftpUrl);
            FixedLengthRecordParser parser = new FixedLengthRecordParser(uncompressedFile.toString(),
                    fixedLengthRecordHeader,
                    bigQueryWriter,
                    programArguments.batchSize);
            parser.parseAndWrite();
            Files.delete(uncompressedFile);
        }

        removeDir(uncompressedDirPath);
        removeDir(compressedDirPath);
    }

    private static void removeDir(Path dir) throws IOException {
        Files.walk(dir)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);

        assert !Files.exists(dir);
    }
}
