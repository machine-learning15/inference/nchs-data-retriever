package works.mars.ml.etl.nchs.app;

import com.google.common.collect.Iterators;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Given a flat file with fixed record length, reads in lines lazily and inserts to datastore in batches
 */
public class FixedLengthRecordParser {

    private final String filePath;
    private final FixedLengthRecordHeader fixedLengthRecordHeader;
    private final RecordWriter recordWriter;
    private final int batchSize;


    public FixedLengthRecordParser(String filePath, FixedLengthRecordHeader fixedLengthRecordHeader, RecordWriter recordWriter, int batchSize) {
        this.filePath = filePath;
        this.fixedLengthRecordHeader = fixedLengthRecordHeader;
        this.recordWriter = recordWriter;
        this.batchSize = batchSize;
    }

    /**
     * Parse lines and batch write
     *
     * @throws IOException
     */
    public void parseAndWrite() throws IOException {

        try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
            Iterators.partition(stream.iterator(), batchSize)
                    .forEachRemaining(this::parseAndWrite);
        }
    }

    private void parseAndWrite(List<String> lines) {

        List<Map<String, String>> rows = lines.parallelStream()
                .map(fixedLengthRecordHeader::parseToFields)
                .collect(Collectors.toUnmodifiableList());

        if (!recordWriter.insertRecords(rows)) {
            System.out.println("Problem writing records...will exit");
            System.exit(1);
        }
    }
}
