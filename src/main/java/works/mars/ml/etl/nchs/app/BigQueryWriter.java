package works.mars.ml.etl.nchs.app;

import com.google.cloud.bigquery.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * Given a dataset and a table name, inserts records to BigQuery in batch.
 * If the dataset or the table does not exist, creates them with the schema inferred from the metadata file
 */
public class BigQueryWriter implements RecordWriter {

    private final String datasetName;
    private final String tableName;
    private final TableId tableId;
    private final BigQuery bigQuery;
    private int recordCount = 0;
    private final List<FixedLengthField> fields;


    public BigQueryWriter(String datasetName,
                          String tableName,
                          List<FixedLengthField> fields) {

        this.datasetName = datasetName;
        this.tableName = tableName;
        this.tableId = TableId.of(this.datasetName, this.tableName);
        this.fields = fields;

        bigQuery = BigQueryOptions.getDefaultInstance().getService();

        if (bigQuery.getDataset(datasetName) == null) {
            System.out.printf("Dataset = %s does not exist, creating dataset and table = %s%s", datasetName, tableName, System.lineSeparator());
            createDataset();
            createTable();
        } else if (bigQuery.getTable(datasetName, tableName) == null) {
            System.out.printf("Table = %s does not exist, creating table = %s%s", tableName, tableName, System.lineSeparator());
        }
    }

    @Override
    public boolean insertRecords(Collection<Map<String, String>> records) {

        InsertAllRequest.Builder builder = InsertAllRequest.newBuilder(tableId);

        for (Map<String, String> rowContent : records) {
            recordCount++;
            builder.addRow(rowContent);
        }

        InsertAllResponse response = bigQuery.insertAll(builder.build());

        if (response.hasErrors()) {
            // If any of the insertions failed, this lets you inspect the errors
            for (Map.Entry<Long, List<BigQueryError>> entry : response.getInsertErrors().entrySet()) {
                System.err.println(entry);
            }

            return false;
        } else {
            System.out.printf("%d: %s: Wrote to BQ count : %d\n", Thread.currentThread().getId(),
                    LocalDateTime.now().toString(),
                    recordCount);

            return true;
        }
    }


    private void createDataset() {

        DatasetInfo datasetInfo = DatasetInfo.newBuilder(datasetName).build();
        Dataset dataset = bigQuery.create(datasetInfo);

        System.out.printf("Dataset %s created.%n", dataset.getDatasetId().getDataset());
    }

    private void createTable() {

        Schema schema = generateSchema();

        TableId tableId = TableId.of(datasetName, tableName);
        TableDefinition tableDefinition = StandardTableDefinition.of(schema);
        TableInfo tableInfo = TableInfo.newBuilder(tableId, tableDefinition).build();

        bigQuery.getDataset(datasetName);
        bigQuery.create(tableInfo);
        System.out.println("Table created successfully");
    }


    private Schema generateSchema() {

        List<Field> bigQueryFields = new ArrayList<>(fields.size());

        for (FixedLengthField field : fields) {
            bigQueryFields.add(Field.of(field.getName(), StandardSQLTypeName.STRING));
        }

        return Schema.of(bigQueryFields);
    }

}
