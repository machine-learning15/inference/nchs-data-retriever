package works.mars.ml.etl.nchs.app;

import java.util.Objects;

/**
 * Some fields might have only one character; for those, start and end indices are the same value
 */
public class FixedLengthField implements Comparable<FixedLengthField> {

    private final String name;
    private final Integer start;
    private final Integer end;

    public FixedLengthField(String name, Integer start, Integer end) {

        if (name == null || name.trim().length() == 0) {
            throw new IllegalArgumentException("Field name must have at least 1 character, but given = " + name);
        }

        if (start < 0 || end < 0 || end < start) {
            throw new IllegalArgumentException(String.format("End index must start after start index, but given start = %d, end = %d fieldName = %s", start, end, name));
        }

        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public Integer getStart() {
        return start;
    }

    public Integer getEnd() {
        return end;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, start, end);
    }

    @Override
    public int compareTo(FixedLengthField o) {
        return this.start.compareTo(o.start);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FixedLengthField field = (FixedLengthField) o;
        return name.equals(field.name) &&
                start.equals(field.start) &&
                end.equals(field.end);
    }

    @Override
    public String toString() {
        return "Field{" +
                "\"name\":\"" + name + "\"" +
                ", \"start\":" + start +
                ", \"end\":" + end +
                "}";
    }
}
