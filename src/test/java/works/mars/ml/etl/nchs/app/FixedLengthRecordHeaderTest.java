package works.mars.ml.etl.nchs.app;

import org.junit.Before;
import org.junit.Test;


import java.util.HashMap;

public class FixedLengthRecordHeaderTest {

    private FixedLengthRecordHeader fileStructure;
    private HashMap<String, Integer[]> input;

    @Before
    public void setUp()  {
        input = new HashMap<>();

    }

    @Test(expected = IllegalArgumentException.class)
    public void overlappingFields_shouldThrowException()
    {
        addToInput(new FixedLengthField("a", 2,4));
        addToInput(new FixedLengthField("b", 6,7));
        addToInput(new FixedLengthField("c", 4,7));

        fileStructure = new FixedLengthRecordHeader(input);
    }

    private void addToInput(FixedLengthField f)
    {
        input.put(f.getName(), new Integer[]{f.getStart(), f.getEnd()});
    }
}