package works.mars.ml.etl.nchs.app;

import org.junit.Before;
import org.junit.Test;
import works.mars.ml.etl.nchs.app.NCHSDataDownloader;

import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

import static org.junit.Assert.*;

public class NCHSDataDownloaderTest {

    NCHSDataDownloader dataDownloader;

    @Before
    public void setup()
    {
        dataDownloader = new NCHSDataDownloader(Path.of("test-uncompressed"), Path.of("test-compressed"));
    }

    @Test
    public void getURL() throws MalformedURLException {

        URL actual = dataDownloader.getURL("ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.zip");
        assertEquals("/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.zip", actual.getPath());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getURL_throwsException_whenHostDoesNotMatch() throws MalformedURLException {
        dataDownloader.getURL("ftp://ftp.wrong.gov/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.zip");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getURL_throwsException_whenProtocolDoesNotMatch() throws MalformedURLException {
        dataDownloader.getURL("http://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.zip");
    }

    @Test(expected = IllegalArgumentException.class)
    public void getURL_throwsException_whenExtensionDoesNotMatch() throws MalformedURLException {
        dataDownloader.getURL("http://ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.tar");
    }

    @Test(expected = java.net.MalformedURLException.class)
    public void getURL_throwsException_whenMalformedUrl() throws MalformedURLException {
        dataDownloader.getURL("ftp.cdc.gov/pub/Health_Statistics/NCHS/Datasets/DVS/natality/Nat2018ps.tar");
    }

}