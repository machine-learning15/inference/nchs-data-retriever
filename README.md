### CDC NCHS Data Retriever
A utility app that retrieves a list of compressed fixed-length format files from [the National Vital Statistics System](https://www.cdc.gov/nchs/data_access/vitalstatsonline.htm), and writes to BigQuery in batch with a schema.  Database Schema is auto-generated from [metadata file](birthDataMetadata2018-2015.json).

#### App arguments
```
--ftp-file-urls Space delimited ftp urls
--path-to-metadata-file File containing field names and their start and end indices.  If the indices start do not start counting from 0, set --startCountingFromOne
--batch-size Number of records that will be processed in batch
--dataset-name BigQuery dataset name, if dataset does not exist, it will be created
--table-name BigQuery table name where records are written in batch, if table does not exists, it will be created
--expectedFieldCount Number of fields in metadata file
--startCountingFromOne Whether the indices given in the metadata file start counting from 0.  NCHS starts counting from 1.
```

#### Metadata file structure
For each field to be included, a list of start and end index, e.g: "DOB_YY": [9,12]
If a field occupies only 1 character, list has only one index. e.g "F_MHISP": [116]

The app will check these indices and throw an exception if more than 1 field occupy the same indices.  These indices are retrieved from
User guide.pdf files provided by CDC. Here is an [example](ftp://ftp.cdc.gov/pub/Health_Statistics/NCHS/Dataset_Documentation/DVS/natality/UserGuide2018-508.pdf).



